import "./App.css";
import { Component } from 'react';
import React from "react";
import { BrowserRouter , Switch, Route } from "react-router-dom";
import NavBar from './components/NavBar';
import Cart from './components/Cart'; 
import ItemListContainer from './components/ItemListContainer'; 
import ItemDetailContainer from './components/ItemDetailContainer'; 
//import FinalizarCompra from "./components/FinalizarCompra/FinalizarCompra";
import {CartProvider} from "./context/CartContext"


class App extends Component {
  render(){
  return (<>
  <CartProvider>
    <BrowserRouter>
      <NavBar/>
    <Switch>
      <Route exact path="/">
        <ItemListContainer/>
      </Route>
      <Route path="/category/:idCategoria">
        <ItemListContainer/>
      </Route>
      <Route path="/item/:itemId">
        <ItemDetailContainer/>
      </Route>
      <Route path="/cart">
        <Cart/>
      </Route>
    </Switch>
    </BrowserRouter>
    </CartProvider>
    </>
  );
  }
}

export default App;
