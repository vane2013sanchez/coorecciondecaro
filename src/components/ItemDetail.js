import { Button } from "semantic-ui-react";
import "./componentsStyles/itemDetailStyle.css";
import ItemCounter from './ItemCounter.js';
import React, { useState } from "react";
import { useCartContext } from "../context/CartContext";
import {Link} from "react-router-dom";

const ItemDetail = function({producto}){
    console.log("item detail",producto);

    const [count, setCount] = useState(0);
    const {addToCart} = useCartContext();
    

    console.log("mi producto desectru",producto);
    const agregar = (contador) => {
        setCount(contador);               
    };
   
    function modificarEvent(){
        setCount(0);
    }
    function terminarCompra(){
        addToCart(producto, count);
    }
    return (<div className="det">
        
        <div className="detalle">
            <p>{producto[0]?.nombre}</p> 
            <p>{producto[0]?.descripcion}</p> 
            <p>${producto[0]?.precio}</p>
            <p>Stock: {producto[0]?.stock}</p>
            
            
            {count!==0 ? <div> <div className="cantidad">Cantidad solicitada: {count}</div>
            <Link to="/cart"><Button className="btnTerminarCompra" onClick={terminarCompra}>Terminar mi compra</Button></Link>
            <Button className="btnModificar" onClick={modificarEvent}>Modificar cantidad</Button></div>:<div>
            <ItemCounter stock={producto.id} onAdd={agregar}/>
            </div>}
            
           
            
        </div>
        <img src={producto.img} alt=""/>
        
    </div>)
}
export default ItemDetail;